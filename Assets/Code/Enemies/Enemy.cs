﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	#region events

	public delegate void MoveCompleteHandler (GameObject myObj);
	public event MoveCompleteHandler MoveCompelteEvent;

	#endregion

	#region private vars

	private GameObject _corpse;
	private GameObject _sprite;

	#endregion

	#region monobehaviour inherited

	void Awake() {
		_corpse = transform.Find ("dead").gameObject;
		_corpse.SetActive (false);

		_sprite = transform.Find ("sprite").gameObject;
		_sprite.SetActive (true);
	}

	#endregion

	#region public methods

	public void Move(Vector3 newPos) {

		Go.to (transform, 0.2f, new GoTweenConfig()
		       .setEaseType(GoEaseType.Linear)
		       .localPosition(newPos)
		       .onComplete(MoveComplete)
		       );

	}

	public void Die(AttackDefs.DamageType type) {
		_sprite.SetActive (false);
		_corpse.SetActive (true);
	}

	#endregion

	#region private methods

	void DeathEnd(AbstractGoTween tween) {

	}

	void MoveComplete(AbstractGoTween tween) {
		if (MoveCompelteEvent != null) {
			MoveCompelteEvent(gameObject);
		}
	}

	#endregion
}
