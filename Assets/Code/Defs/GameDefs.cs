﻿using UnityEngine;
using System.Collections;

public static class GameDefs {

	public const string kHighScoreSaveKey = "HighScoreMatchKingdoms_KEY";

	public static int GetEnemyAmountForWave(int wave) {
		return wave * wave;
	}
}
