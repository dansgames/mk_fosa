﻿using UnityEngine;
using System.Collections;

public static class AttackDefs {

	#region public types

	public enum DamageType {
		sword,
		bow,
		magic
	}

	#endregion

	#region public methods
	
	public static float GetDamageForMatchPoints(DamageType type, int matchPoints) {
		switch (type) {
		case DamageType.sword:
			return matchPoints*3f;
			
		case DamageType.bow:
			return matchPoints*1.5f;
		}
		
		return 0f;
	}
	
	public static float GetDamageForMagicAttack(int wave) {
		return 2 * wave;
	}

	#endregion
}
