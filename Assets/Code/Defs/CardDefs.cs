﻿using UnityEngine;
using System.Collections;

public static class CardDefs {

	public enum eCardType {
		bow,
		shield,
		sword,
		tree,
		magic
	}
	
	public static Vector2 CardSize = new Vector2(0.48f, 0.46f);
	public const string CardPrefabName = "Cards/CardPrefab";
}