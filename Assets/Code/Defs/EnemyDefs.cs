﻿using UnityEngine;
using System.Collections;

public static class EnemyDefs
{
	public const string kEnemyPrefabName = "Enemies/EnemyPrefab";

	public static Vector3 enemySpawnStartPosition = new Vector3(0f, 2f, 0f);

	public const float enemySpacing = 0.25f;
	public const float enemyMoveDistance = 0.25f;
	public const float enemyAttackHeight = 0.05f;
}
