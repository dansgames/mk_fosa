﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyMaster : MonoBehaviour {

	#region events

	public delegate void EnemyMasterMoveEventHandler (int enemiesBreaching);
	public event EnemyMasterMoveEventHandler EnemyMasterMoveCompleteEvent;

	public delegate void EnemyDiedEventHandler();
	public event EnemyDiedEventHandler EnemyDiedEvent;

	#endregion

	#region public interface

	public bool bWaveComplete {
		get { return _bWaveComplete; }
	}

	#endregion

	#region private variables

	private List<GameObject> _enemies = new List<GameObject>();
	private bool _bWaveComplete = true;

	private int _breached = 0;

	private List<GameObject> _movingEnemies = new List<GameObject>();
	private List<GameObject> _deadEnemies = new List<GameObject>();

	#endregion

	#region public methods

	public void CreateWave(int wave) {

		int enemyCount = GameDefs.GetEnemyAmountForWave (wave);

		if (enemyCount > 100)
			enemyCount = 100;

		int height = Mathf.CeilToInt((Mathf.Sqrt(enemyCount)));
		int width = enemyCount/height;

		for (int iy = 0; iy < height; iy++) {
			for (int ix = 0; ix < width; ix++) {

				Vector3 spawnPosition = EnemyDefs.enemySpawnStartPosition;
				spawnPosition.z += iy*0.01f;

				float space = ix*EnemyDefs.enemySpacing;
				float offset = ((width-1f)/2f)*EnemyDefs.enemySpacing;

				spawnPosition.x += space-offset;
				spawnPosition.y += EnemyDefs.enemySpacing*iy;

				GameObject newEnemy = GameObject.Instantiate (Resources.Load (EnemyDefs.kEnemyPrefabName)) as GameObject;
				_enemies.Add (newEnemy);
				
				newEnemy.transform.position = spawnPosition;
			}
		}

		_bWaveComplete = false;
	}

	public void TakeDamage(float damage, AttackDefs.DamageType type) {

//		Debug.Log ("<color=cyan> Took Damage: "+damage+" </color>");

		for (int i=0; i<damage; i++) {

			if (_enemies.Count > 0) {
//				Debug.Log ("<color=white> Destroying an enemy </color>");
				
				GameObject enemy = _enemies [0];
				_enemies.RemoveAt (0);

				enemy.GetComponent<Enemy>().Die(type);
				_deadEnemies.Add(enemy);

				if(EnemyDiedEvent != null)
					EnemyDiedEvent();

				if (_enemies.Count < 1) {
					CleanUpDead();
					_bWaveComplete = true;
				}

			} else {
				CleanUpDead();
				_bWaveComplete = true;
			}
		}
	}

	public void Move() {
		for (int i=_enemies.Count-1; i>-1; i--) {
			Vector3 newPos = _enemies[i].transform.position;

			newPos.y -= EnemyDefs.enemyMoveDistance;

			_movingEnemies.Add(_enemies[i]);
			Enemy enemy = _enemies[i].GetComponent<Enemy>();
			enemy.MoveCompelteEvent += HandleMoveCompelteEvent;
			enemy.Move(newPos);
		}
	}

	void HandleMoveCompelteEvent (GameObject enemy)
	{
		enemy.GetComponent<Enemy> ().MoveCompelteEvent -= HandleMoveCompelteEvent;
		_movingEnemies.Remove (enemy);
		
		if(enemy.transform.position.y <= EnemyDefs.enemyAttackHeight) {
			_breached++;
			_enemies.Remove(enemy);
			Destroy (enemy);
		}
		
		if (_movingEnemies.Count < 1) {
			AllEnemiesHaveMoved();
		}
	}

	#endregion

	#region private methods

	void AllEnemiesHaveMoved() {

		if (_enemies.Count < 1) {
			CleanUpDead();
			_bWaveComplete = true;
		}
		
		if (EnemyMasterMoveCompleteEvent != null)
			EnemyMasterMoveCompleteEvent (_breached);

		_breached = 0;
	}

	void CleanUpDead() {
		foreach (var en in _deadEnemies) {
			GameObject.Destroy(en);
		}

		_deadEnemies.Clear ();
	}

	#endregion

	#region event handlers

	void HandleMoveCompleteEvent() {

	}

	#endregion
}
