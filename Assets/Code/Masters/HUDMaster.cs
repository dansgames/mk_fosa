﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDMaster : MonoBehaviour {

	#region private fields

	// hud elements
	private Text _txtWave;
	private Text _txtDefense;
	private Text _txtMp;

	// screens
	private GameObject _startScreen;
	private GameObject _endScreen;
	private GameObject _tutScreen;

	private GameObject _btnMagic;

	// Start Screen Elements
	private GameObject _ssBestTitle;
	private Text _ssHighPoints;

	// End Screen Elements
	private Text _endPoints;
	private Text _endScore;
	private Text _endBest;
	private Text _endNewHigh;

	#endregion

	public void Init() {
		// Hud
		_txtWave = GameObject.Find ("txtWave").GetComponent<Text> ();
		_txtDefense = GameObject.Find ("txtDefense").GetComponent<Text> ();
		_txtMp = GameObject.Find ("txtMp").GetComponent<Text> ();

		_btnMagic = GameObject.Find ("btnMagic").gameObject;

		// StartScreen
		_startScreen = GameObject.Find ("StartScreen").gameObject;
		_ssBestTitle = GameObject.Find ("ssBest").gameObject;
		_ssHighPoints = GameObject.Find ("ssScore").GetComponent<Text> ();



		// EndScreen
		_endScreen = GameObject.Find ("EndScreen").gameObject;

		// TutScreen
		_tutScreen = GameObject.Find ("TutScreen").gameObject;
		_endPoints = GameObject.Find ("esPoints").GetComponent<Text> ();
		_endScore = GameObject.Find ("esScore").GetComponent<Text> ();
		_endBest = GameObject.Find ("esBest").GetComponent<Text> ();
		_endNewHigh = GameObject.Find ("esNewHigh").GetComponent<Text> ();

		_endScreen.SetActive (false);
		_tutScreen.SetActive (false);

		ShowHud (false);
		ShowMagicButton (false);
	}

	#region public methods

	public void NewGame() {
		ShowHud (true);
	}

	public void ShowMagicButton(bool toggle) {
		_btnMagic.SetActive(toggle);
	}

	public void ShowHud(bool toggle) {
		if (toggle) {
			_txtWave.enabled = true;
			_txtDefense.enabled = true;
			_txtMp.enabled = true;
		} else {
			_txtWave.enabled = false;
			_txtDefense.enabled = false;
			_txtMp.enabled = false;
		}
	}

	public void UpdateHud(int wave, int defense, int mp) {
		_txtWave.text = "Wave: " + wave;
		_txtDefense.text = "Defense: " + defense;
		_txtMp.text = "MP: " + mp + "/10";
	}

	public void ShowTutScreen(bool toggle) {
		_tutScreen.SetActive (toggle);
	}

	public void SetupStartScreen(bool hasHighScore, int score = 0) {
		if (hasHighScore) {
			_ssHighPoints.text = score.ToString ();
		} else {
			_ssHighPoints.gameObject.SetActive(false);
			_ssBestTitle.SetActive(false);
		}
	}

	public void ShowStartScreen(bool toggle) {
		_startScreen.SetActive (toggle);
	}

	public void SetupEndScreenAndShow(int wave, int kills, int matches, int prevHighScore) {

		int score = kills + wave + matches;

		Debug.Log ("score in hud: " + score);
		Debug.Log ("prevscore in hud: " + prevHighScore);

		_endPoints.text = 	"Wave: " + wave + " \n" +
							"Kills: " + kills + " \n" +
							"Tiles Matched: " + matches + " \n\n" +

							"Add together for overall score:";

		if (score > prevHighScore) {
			_endNewHigh.gameObject.SetActive (true);
			prevHighScore = score;
		} else {
			_endNewHigh.gameObject.SetActive (false);
		}

		_endScore.text = score.ToString();
		_endBest.text = "Best: " + prevHighScore;

		_endScreen.SetActive (true);
	}

	#endregion
}
