﻿using UnityEngine;
using System.Collections;

public class InputMaster : MonoBehaviour {

	#region Events

	public delegate void SwipeRegistered(eSwipeDirection direction, Card card);
	public event SwipeRegistered SwipeRegisteredEvent;

	#endregion

	#region private fields

	private bool _bMoving = false;
	private GameObject _go;
	private Card _card;

	private Vector3 _startPosition;

	#endregion

	#region MonoBehaviour Inherited

	// Update is called once per frame
	void Update () {

		if(Input.GetMouseButtonDown(0) && !_bMoving) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit = new RaycastHit();

			_bMoving = Physics.Raycast (ray, out hit) && hit.transform.GetComponent<Card>();
			if(_bMoving)
			{
				_card = hit.transform.GetComponent<Card>();
				_card.Activate(true);
				_go = hit.transform.gameObject;
				_startPosition = Input.mousePosition;
//				Debug.Log("Touch Detected on : "+_go.name);
			}
		}
		
		if (Input.GetMouseButtonUp(0) && _go) {
			Release();
		}

		if (_bMoving) {

			if (Vector2.Distance(Input.mousePosition, _startPosition) > 50f) {

				Vector3 normal = (Input.mousePosition-_startPosition).normalized;

				if (normal.x > 0) {

					if(normal.x >= 0.5f) {
						if(SwipeRegisteredEvent != null) {
							SwipeRegisteredEvent(eSwipeDirection.Right, _card);
						}
					} else {
						if(normal.y > 0.5f) {

							if(SwipeRegisteredEvent != null) {
								SwipeRegisteredEvent(eSwipeDirection.Up, _card);
							}

						} else if ( normal.y < -0.5f) {

							if(SwipeRegisteredEvent != null) {
								SwipeRegisteredEvent(eSwipeDirection.Down, _card);
							}

						}
					}

				} else if (normal.x <= 0) {
					if(normal.x < -0.5f) {
						if(SwipeRegisteredEvent != null) {
							SwipeRegisteredEvent(eSwipeDirection.Left, _card);
						}
					} else {
						if(normal.y > 0.5f) {
							
							if(SwipeRegisteredEvent != null) {
								SwipeRegisteredEvent(eSwipeDirection.Up, _card);
							}
							
						} else if ( normal.y < -0.5f) {
							
							if(SwipeRegisteredEvent != null) {
								SwipeRegisteredEvent(eSwipeDirection.Down, _card);
							}
							
						}
					}
				}

				Release();
			}
		}
	}

	#endregion

	#region private methods

	void Release() {
		_bMoving = false;
		_card.Activate(false);
		_card = null;
		_go = null;
	}

	#endregion
}
