﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardMaster : MonoBehaviour {

	#region events

	public delegate void MatchEventHandler ();
	public event MatchEventHandler MatchFailedEvent;
	public event MatchEventHandler MatchCompleteEvent;

	public delegate void MatchFoundEventHandler(CardDefs.eCardType typeOfMatch, int size);
	public event MatchFoundEventHandler MatchFoundEvent;

	#endregion

	#region private variables

	private Dictionary<Vector2, Card> _tiles = new Dictionary<Vector2, Card>();
	private bool bZeroMatchesFound = false;
	private bool bHasSpaces = true;

	#endregion

	#region Public methods

	public void ToggleViewBoard(bool toggle) {

		foreach (var tile in _tiles) {
			tile.Value.gameObject.SetActive(toggle);
		}
	}

	public void SetupRandom() {
//		CreateBoardPieceForTarget (new Vector2 (0, 0), CardDefs.eCardType.bow);
//		CreateBoardPieceForTarget (new Vector2 (0, 1), CardDefs.eCardType.sword);
//		CreateBoardPieceForTarget (new Vector2 (1, 0), CardDefs.eCardType.sword);

		for (int ix=0; ix<BoardDefs.boardSizeWidth; ix++) {
			for (int iy=0; iy<BoardDefs.boardSizeHeight; iy++) {

				CardDefs.eCardType type = CardDefs.eCardType.bow;

				int rand = Random.Range(0, 150);
				if(rand < 33 )
					type = CardDefs.eCardType.bow;
				if(rand > 32 )
					type = CardDefs.eCardType.sword;
				if(rand > 66 )
					type = CardDefs.eCardType.shield;
				if(rand > 100 )
					type = CardDefs.eCardType.tree;
				if(rand > 125 )
					type = CardDefs.eCardType.magic;



				Vector2 checkVec = new Vector2(ix-1, iy);
				Card cardToCheck;

				if(_tiles.TryGetValue(checkVec, out cardToCheck)) {

					if(cardToCheck) {
						if(cardToCheck.cardType == type) {
							if((int)type >3) {
								type = (CardDefs.eCardType)0;
							} else {
								type = (CardDefs.eCardType)((int)type)+1;
							}
						}
					}
				}

				checkVec = new Vector2(ix, iy-1);

				if(_tiles.TryGetValue(checkVec, out cardToCheck)) {
					
					if(cardToCheck) {
						if(cardToCheck.cardType == type) {
							if((int)type >3) {
								type = (CardDefs.eCardType)0;
							} else {
								type = (CardDefs.eCardType)((int)type)+1;
							}
						}
					}
				}
				
				GameObject newPiece = CreateBoardPieceForTarget (new Vector2 (ix, iy), type);
				_tiles.Add (new Vector2 (ix, iy), newPiece.GetComponent<Card>());
			}
		}
	}

	public void TrySwapTile(Vector2 tileA, eSwipeDirection dir) {

//		Debug.Log ("Swap :" + tileA);

		if (tileA.x == 0 && dir == eSwipeDirection.Left) {
			if (MatchFailedEvent != null)
				MatchFailedEvent();

			return;
		}

		if (tileA.x == 5 && dir == eSwipeDirection.Right) {
			if (MatchFailedEvent != null)
				MatchFailedEvent();
			
			return;
		}

		if (tileA.y == 0 && dir == eSwipeDirection.Up) {
			if (MatchFailedEvent != null)
				MatchFailedEvent();
			
			return;
		}

		if (tileA.y == 4 && dir == eSwipeDirection.Down) {
			if (MatchFailedEvent != null)
				MatchFailedEvent();
			
			return;
		}

		StartCoroutine(SwapTile (tileA, dir));
	}

	#endregion

	#region Private Methods

	IEnumerator SwapTile(Vector2 tileA, eSwipeDirection dir) {

		Vector2 tileB = tileA;
		Card cardA;
		Card cardB;

		if (_tiles.TryGetValue(tileA, out cardA)) {
			if(cardA) {
				switch(dir) {
				case eSwipeDirection.Down:
					tileB.y += 1;
					break;
				case eSwipeDirection.Left:
					tileB.x -= 1;
					break;
				case eSwipeDirection.Right:
					tileB.x += 1;
					break;
				case eSwipeDirection.Up:
					tileB.y -= 1;
					break;
				}

				if (_tiles.TryGetValue(tileB, out cardB)) {

					if (cardB) {

//						Debug.Log ("<color=green> Match Found! </color>");
						
						Vector2 oldAPos = cardA.boardPosition;

						cardA.TileSwapped(cardB.boardPosition);
						cardB.TileSwapped(oldAPos);

						cardA.transform.localPosition = BoardUtils.GetWorldPositionForBoardPosition(cardA.boardPosition);
						cardB.transform.localPosition = BoardUtils.GetWorldPositionForBoardPosition(cardB.boardPosition);

						_tiles[tileA] = cardB;
						_tiles[tileB] = cardA;
						
						while(!bZeroMatchesFound) {
							yield return StartCoroutine(CheckMatches());
						}
						bZeroMatchesFound = false;
//						Debug.Log("<color=orange> @@@@@@@@ DONE DONE DONE </color>");
						if (MatchCompleteEvent != null)
							MatchCompleteEvent();

					} else {
						if (MatchFailedEvent != null)
							MatchFailedEvent();
						
						yield break;
					}
				}

			} else {
				if (MatchFailedEvent != null)
					MatchFailedEvent();
				
				yield break;
			}
		}
	}

	IEnumerator CheckMatches() {

		int checkNo = 0;

		Card prevCard = null;
		List<Card> matchCards = new List<Card> ();
		List<Card> definiteMatches = new List<Card> ();
		List<Vector2> spaces = new List<Vector2> ();

		for (int iy=0; iy<BoardDefs.boardSizeHeight; iy++) 
		{
			for (int ix=0; ix<BoardDefs.boardSizeWidth; ix++) {
				
				Card checkCard = _tiles[new Vector2(ix, iy)];
//				Debug.Log ("checking: " + checkCard + "type: " + checkCard.cardType + " prev: " + prevCard);
				checkNo++;
				if(prevCard) {
					
					if(prevCard.cardType == checkCard.cardType) {
						
						if(!matchCards.Contains(checkCard)) {
//							Debug.Log ("<color=cyan> TestAdd: " + checkCard + "</color>");
							matchCards.Add (checkCard);
						}
						
						if(!matchCards.Contains(prevCard)) {
							matchCards.Add (prevCard);
//							Debug.Log ("<color=cyan> TestAdd: " + prevCard + "</color>");
						}
					} else {
						
//						Debug.Log ("END : " + matchCards.Count);
						
						if(matchCards.Count > 2) {
							foreach(var card in matchCards) {
								if(!definiteMatches.Contains(card)) {
//									Debug.Log ("<color=red> DEF ADD: " + card + "</color>");
									definiteMatches.Add (card);
								}
							}
						}
						if(MatchFoundEvent != null && matchCards.Count>2)
							MatchFoundEvent(matchCards[0].cardType, matchCards.Count);

						matchCards.Clear();
					}
					
					prevCard = checkCard;
					
				} else {
					prevCard = checkCard;
				}
			}
			
			if(matchCards.Count > 2) {
				foreach(var card in matchCards) {
					if(!definiteMatches.Contains(card)) {
//						Debug.Log ("<color=red> DEF ADD: " + card + "</color>");
						definiteMatches.Add (card);
					}
				}
			}
			if(MatchFoundEvent != null && matchCards.Count>2)
				MatchFoundEvent(matchCards[0].cardType, matchCards.Count);
			matchCards.Clear();
			prevCard = null;
		}

//		Debug.Log (" +++++ VERTICAL +++++ ");
		
		for (int ix=0; ix<BoardDefs.boardSizeWidth; ix++) 
		{
			for (int iy=0; iy<BoardDefs.boardSizeHeight; iy++){
				
				Card checkCard = _tiles[new Vector2(ix, iy)];
//				Debug.Log ("checking: " + checkCard + "type: " + checkCard.cardType + " prev: " + prevCard);
				checkNo++;
				if(prevCard) {
					
					if(prevCard.cardType == checkCard.cardType) {
						
						if(!matchCards.Contains(checkCard)) {
//							Debug.Log ("<color=cyan> TestAdd: " + checkCard + "</color>");
							matchCards.Add (checkCard);
						}
						
						if(!matchCards.Contains(prevCard)) {
							matchCards.Add (prevCard);
//							Debug.Log ("<color=cyan> TestAdd: " + prevCard + "</color>");
						}
					} else {
						
//						Debug.Log ("END : " + matchCards.Count);
						
						if(matchCards.Count > 2) {
							foreach(var card in matchCards) {
								if(!definiteMatches.Contains(card)) {
//									Debug.Log ("<color=red> DEF ADD: " + card + "</color>");
									definiteMatches.Add (card);
								}
							}
						}
						if(MatchFoundEvent != null && matchCards.Count>2)
							MatchFoundEvent(matchCards[0].cardType, matchCards.Count);
						matchCards.Clear();
					}
					
					prevCard = checkCard;
					
				} else {
					prevCard = checkCard;
				}
			}
			
			if(matchCards.Count > 2) {
				foreach(var card in matchCards) {
					if(!definiteMatches.Contains(card)) {
//						Debug.Log ("<color=red> DEF ADD: " + card + "</color>");
						definiteMatches.Add (card);
					}
				}
			}

			if(MatchFoundEvent != null && matchCards.Count>2)
				MatchFoundEvent(matchCards[0].cardType, matchCards.Count);

			matchCards.Clear();
			prevCard = null;
		}

		if (definiteMatches.Count > 0) {
			foreach (var card in definiteMatches) {
				card.transform.localScale = Vector3.one*0.75f;
			}

			yield return new WaitForSeconds(0.5f);

			foreach (var card in definiteMatches) {
				
//				Debug.Log("<color=cyan> Clearing: "+card+"</color>");
		
				Vector2 cardPos = card.boardPosition;
				GameObject.Destroy(card.gameObject);
				_tiles.Remove(card.boardPosition);
				spaces.Add(cardPos);
			}

			while (bHasSpaces) {
				yield return StartCoroutine(DropBoard(spaces));
			}
			bHasSpaces = true;

		} else {
			bZeroMatchesFound = true;
		}
//		Debug.Log (bZeroMatchesFound);
//		Debug.Log ("<color=white> PASS DONE "+checkNo+"</color>");
		yield break;
	}

	IEnumerator DropBoard(List<Vector2> spaces) {

		int replaces = 0;

		for (int ix=0; ix<BoardDefs.boardSizeWidth; ix++) {

			for (int iy=BoardDefs.boardSizeHeight-1; iy>0; iy--) {

				Vector2 checkVec = new Vector2(ix, iy);
//				Debug.Log (_tiles.Count);

				if(!_tiles.ContainsKey(checkVec)) {

//					Debug.Log (checkVec + " is empty");

					for (int iyi=iy; iyi>-1; iyi--) {

						if(_tiles.ContainsKey(new Vector2(ix, iyi))) {

//							Debug.Log (checkVec);
//							Debug.Log ("replacement at: " + new Vector2(ix, iyi));

							Card replacement = _tiles[new Vector2(ix, iyi)];

//							Debug.Log (BoardUtils.GetWorldPositionForBoardPosition(checkVec));

//							replacement.transform.localPosition = BoardUtils.GetWorldPositionForBoardPosition(checkVec);

							Go.to (replacement.transform, 0.15f, new GoTweenConfig ()
							       .localPosition(BoardUtils.GetWorldPositionForBoardPosition(checkVec))
							       .setEaseType(GoEaseType.Linear)
							       );

							replacement.TileSwapped(checkVec);
							_tiles.Add(checkVec, replacement);
							_tiles.Remove(new Vector2(ix, iyi));

							yield return new WaitForSeconds(0.025f);
							yield break;
						}
					}

				}
			}
		}
		yield return new WaitForSeconds(0.2f);
		yield return StartCoroutine(CreateNewPieces());
	}

	IEnumerator CreateNewPieces() {
		for (int ix=0; ix<BoardDefs.boardSizeWidth; ix++) {
			for (int iy=BoardDefs.boardSizeHeight-1; iy>-1; iy--) {
				if(!_tiles.ContainsKey(new Vector2(ix, iy)))
				{
					CardDefs.eCardType type = CardDefs.eCardType.bow;
					
					int rand = Random.Range(0, 150);
					if(rand < 33 )
						type = CardDefs.eCardType.bow;
					if(rand > 32 )
						type = CardDefs.eCardType.sword;
					if(rand > 66 )
						type = CardDefs.eCardType.shield;
					if(rand > 100 )
						type = CardDefs.eCardType.tree;
					if(rand > 125 )
						type = CardDefs.eCardType.magic;
					
					GameObject newPiece = CreateBoardPieceForTarget (new Vector2 (ix, iy), type);
					_tiles.Add (new Vector2 (ix, iy), newPiece.GetComponent<Card>());
					yield return new WaitForSeconds(0.025f);
				}
			}
		}
		yield return new WaitForSeconds(0.2f);
		bHasSpaces = false;
	}

	GameObject CreateBoardPieceForTarget(Vector2 boardTargetPosition, CardDefs.eCardType type) {

//		Debug.Log ("CreateBoardPieceForTarget + " + boardTargetPosition);

		Vector3 realWorldTargetPos = BoardUtils.GetWorldPositionForBoardPosition(boardTargetPosition);
		
		GameObject newPiece = GameObject.Instantiate(Resources.Load(CardDefs.CardPrefabName)) as GameObject;
		newPiece.transform.parent = transform;
		Vector3 startPos = realWorldTargetPos;
		startPos.y += 1f;
		newPiece.transform.localPosition = startPos;

		Go.to (newPiece.transform, 0.15f, new GoTweenConfig ()
		       .localPosition(realWorldTargetPos)
		       .setEaseType(GoEaseType.Linear)
		       );

		newPiece.GetComponent<Card> ().Setup (type, boardTargetPosition);

		return newPiece;
	}

	#endregion
}
