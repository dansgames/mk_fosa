﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameMaster : MonoBehaviour {

	#region Private Types

	enum eGameState {
		Paused,
		Ready
	}

	#endregion

	#region private variables

	private eGameState _state = eGameState.Paused;

	private InputMaster _input;
	private BoardMaster _board;
	private EnemyMaster _enemyMaster;
	private HUDMaster	_hud;

	private int _wave = 1;

	private int _defense = 20;
	private int _mp = 0;

	private int _matches = 0;
	private int _kills = 0;

	// Positioning
	private Vector3 _boardPosition = new Vector3(-0.04f, -1.2f, 0);

	private List<KeyValuePair<CardDefs.eCardType, int>> _cardsCleared = new List<KeyValuePair<CardDefs.eCardType, int>>();

	#endregion

	#region Init

	// Use this for initialization
	void Start () {
		GameObject input = new GameObject ();
		input.name = "_input";
		_input = input.AddComponent<InputMaster> ();

		_input.SwipeRegisteredEvent += HandleSwipeRegisteredEvent;

		GameObject board = new GameObject ();
		board.name = "_board";
		_board = board.AddComponent<BoardMaster> ();
		_board.transform.position = _boardPosition;
		_board.SetupRandom ();
		_board.ToggleViewBoard (false);

		GameObject enemySpawner = new GameObject ();
		enemySpawner.name = "_enemySpawner";
		_enemyMaster = enemySpawner.AddComponent<EnemyMaster> ();

		_enemyMaster.EnemyDiedEvent += HandleEnemyDiedEvent;

		GameObject hud = new GameObject ();
		hud.name = "_hud";
		_hud = hud.AddComponent<HUDMaster> ();
		_hud.Init ();

		if (PlayerPrefs.HasKey (GameDefs.kHighScoreSaveKey)) {
			_hud.SetupStartScreen (true, PlayerPrefs.GetInt (GameDefs.kHighScoreSaveKey));
		} else {
			_hud.SetupStartScreen (false);
		}
	}

	#endregion

	#region Public Methods

	/// <summary>
	/// Shows the tutorial screen.
	/// </summary>
	public void ShowTutorial() {
		_hud.ShowStartScreen (false);
		_hud.ShowTutScreen (true);
	}

	/// <summary>
	/// Sets up hud for starting a new game.
	/// </summary>
	public void StartNewGame() {
		_hud.ShowTutScreen (false);
		_board.ToggleViewBoard (true);
		BeginWave();
		_state = eGameState.Ready;
	}

	/// <summary>
	/// Restarts the game.
	/// </summary>
	public void RestartGame() {
		Application.LoadLevel (0);
	}

	/// <summary>
	/// Uses the magic attack.
	/// </summary>
	public void UseMagicAttack() {
		if (_state == eGameState.Ready) {
			_mp = 0;
			_hud.ShowMagicButton(false);
			_enemyMaster.TakeDamage (AttackDefs.GetDamageForMagicAttack (_wave), AttackDefs.DamageType.magic);

			_hud.UpdateHud (_wave, _defense, _mp);
		}
	}

	#endregion

	#region private methods

	/// <summary>
	/// Begins a new wave.
	/// </summary>
	void BeginWave() {
		_enemyMaster.CreateWave (_wave);
		_hud.NewGame ();
		_hud.UpdateHud (_wave, _defense, _mp);
	}

	/// <summary>
	/// Checks the cards cleared for any matches and gets points for them.
	/// then uses those points.
	/// </summary>
	void CheckCardsCleared() {
		// Check Bows
		int bows = CheckCardMatchPoints (CardDefs.eCardType.bow);

		// Check Swords
		int swords = CheckCardMatchPoints (CardDefs.eCardType.sword);

		// Check Shields
		int shields = CheckCardMatchPoints (CardDefs.eCardType.shield);

		int magics = CheckCardMatchPoints (CardDefs.eCardType.magic);

		_mp += magics;
//		Debug.Log ("MP gained: " + magics);

		if (_mp > 10) {
			_mp = 10;
		}

//		Debug.Log (" @@@@@ MP NOW AT : " + _mp);

//		Debug.Log ("Bows cleared: " + bows);
//		Debug.Log ("Swords cleared: " + swords);
//		Debug.Log ("Shields cleared: " + shields);
//		Debug.Log ("Magics cleared: " + magics);

		_cardsCleared.Clear ();

		_enemyMaster.TakeDamage (AttackDefs.GetDamageForMatchPoints (AttackDefs.DamageType.sword, swords), AttackDefs.DamageType.sword);
		_enemyMaster.TakeDamage (AttackDefs.GetDamageForMatchPoints (AttackDefs.DamageType.bow, bows), AttackDefs.DamageType.bow);

		_defense += shields;
		_hud.UpdateHud (_wave, _defense, _mp);

		if (!_enemyMaster.bWaveComplete) {
			_enemyMaster.EnemyMasterMoveCompleteEvent += HandleEnemyMasterMoveCompleteEvent;
			_enemyMaster.Move ();
		} 

		if (_enemyMaster.bWaveComplete) {
//			Debug.Log ("Wave Complete!");
			_wave++;
			_hud.UpdateHud (_wave, _defense, _mp);
			_enemyMaster.CreateWave (_wave);
			_state = eGameState.Ready;

			if(_mp == 10) {
				_hud.ShowMagicButton(true);
			}
		}
	}

	/// <summary>
	/// Checks the card match points.
	/// </summary>
	/// <returns>The card match points.</returns>
	/// <param name="type">Type.</param>
	int CheckCardMatchPoints(CardDefs.eCardType type) {
		int matchPoints = 0;

		foreach (var kvp in _cardsCleared) {
			if(kvp.Key == type) {
				matchPoints+=1;

				if(kvp.Value>3) {
					matchPoints+=1;
				}

				if(kvp.Value>4) {
					matchPoints+=3;
				}
			}
		}

		return matchPoints;
	}

	/// <summary>
	/// Called when Dead. Ends the game. Shows Game Over Screen.
	/// </summary>
	void EndGame() {
		_board.ToggleViewBoard (false);
		int prevHighScore = 0;
		int score = _wave+_kills+_matches;

		if (!PlayerPrefs.HasKey (GameDefs.kHighScoreSaveKey)) {
//			Debug.Log ("no previous");
			PlayerPrefs.SetInt (GameDefs.kHighScoreSaveKey, (_wave + _kills + _matches));
		}

		prevHighScore = PlayerPrefs.GetInt(GameDefs.kHighScoreSaveKey);
//		Debug.Log ("score: " + score);
//		Debug.Log ("prrevious : " + prevHighScore);

		if(prevHighScore < score) {
			PlayerPrefs.SetInt (GameDefs.kHighScoreSaveKey, (_wave + _kills + _matches));
		}

		_hud.ShowHud(false);
		_hud.SetupEndScreenAndShow (_wave, _kills, _matches, prevHighScore);

	}

	#endregion

	#region Event Handlers

	/// <summary>
	/// Handles the enemy died event.
	/// Updates kill count
	/// </summary>
	void HandleEnemyDiedEvent () {
		_kills++;
	}

	/// <summary>
	/// Handles the enemy master move complete event.
	/// Effectively ending the turn.
	/// </summary>
	/// <param name="breached">Breached.</param>
	void HandleEnemyMasterMoveCompleteEvent (int breached)
	{
		_enemyMaster.EnemyMasterMoveCompleteEvent -= HandleEnemyMasterMoveCompleteEvent;

		_defense -= breached;

		if (_defense <= 0) {

			EndGame();

		} else {
			_hud.UpdateHud (_wave, _defense, _mp);
			
//			Debug.Log ("Turn Complete - Breaches: "+breached);
			_state = eGameState.Ready;

			if(_mp == 10) {
				_hud.ShowMagicButton(true);
			}
		}
	}

	/// <summary>
	/// Handles the swipe registered event.
	/// Sends the swipe and position to the baord master.
	/// </summary>
	/// <param name="direction">Direction.</param>
	/// <param name="card">Card.</param>
	void HandleSwipeRegisteredEvent (eSwipeDirection direction, Card card)
	{
		if (_state == eGameState.Paused)
			return;

		_hud.ShowMagicButton(false);

//		Debug.Log ("Got Swipe: " + direction);
		_state = eGameState.Paused;

		_board.MatchFailedEvent += HandleMatchFailedEvent;
		_board.MatchCompleteEvent += HandleMatchCompleteEvent;
		_board.MatchFoundEvent += HandleMatchFoundEvent;

		_board.TrySwapTile (card.boardPosition, direction);
	}

	/// <summary>
	/// Handles the match found event.
	/// </summary>
	/// <param name="typeOfMatch">Type of matched cards.</param>
	/// <param name="size">Size of match 3/4/5.</param>
	void HandleMatchFoundEvent (CardDefs.eCardType typeOfMatch, int size)
	{
//		Debug.Log ("Got Match Type: " + typeOfMatch + " size: " + size);

		_matches++;

		_cardsCleared.Add (new KeyValuePair<CardDefs.eCardType, int> (typeOfMatch, size));
	}

	/// <summary>
	/// Handles the match complete event.
	/// </summary>
	void HandleMatchCompleteEvent ()
	{
		_board.MatchFailedEvent -= HandleMatchFailedEvent;
		_board.MatchCompleteEvent -= HandleMatchCompleteEvent;
		_board.MatchFoundEvent -= HandleMatchFoundEvent;

		CheckCardsCleared ();
	}

	/// <summary>
	/// Handles the match failed event.
	/// </summary>
	void HandleMatchFailedEvent ()
	{
		_board.MatchFailedEvent -= HandleMatchFailedEvent;
		_board.MatchCompleteEvent -= HandleMatchCompleteEvent;
		_board.MatchFoundEvent -= HandleMatchFoundEvent;

//		Debug.Log ("Match Failed");
		_state = eGameState.Ready;
	}

	#endregion
}
