﻿using UnityEngine;
using System.Collections;

public static class BoardUtils
{
	public static Vector2 GetWorldPositionForBoardPosition(Vector2 boardPosition) {
//		Debug.Log ("board position: " + boardPosition);

		Vector2 boardOffset = new Vector2(-((BoardDefs.boardSizeWidth/2.5f)*CardDefs.CardSize.x), (BoardDefs.boardSizeHeight/2)*CardDefs.CardSize.y);

//		Debug.Log ("board offset: " + boardOffset);

		float x = boardPosition.x * CardDefs.CardSize.x;
//		Debug.Log ("x: " + x);

		float y = -boardPosition.y * CardDefs.CardSize.y;
//		Debug.Log ("y: " + y);

		return boardOffset+new Vector2 (x, y);

	}
}
